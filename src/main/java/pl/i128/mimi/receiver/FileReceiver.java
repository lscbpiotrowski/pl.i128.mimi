package pl.i128.mimi.receiver;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import com.vaadin.server.NoOutputStreamException;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

import pl.i128.mimi.entity.Files;

public class FileReceiver implements Receiver, SucceededListener, FailedListener {
	static final long serialVersionUID = 8907596907442731009L;

	private File file;
	private long size;

	private List<Files> fileList;
	private Grid grid;

	public FileReceiver(List<Files> fileList) {
		this.fileList = fileList;
	}

	public FileReceiver(List<Files> fileList, Grid<Files> grid) {
		this.fileList = fileList;
		this.grid = grid;
	}

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		FileOutputStream fileOutputStream = null;

		this.file = new File("/tmp/" + filename);

		try {

			fileOutputStream = new FileOutputStream(file);

		} catch (FileNotFoundException e) {
			new Notification("Could not open file", Notification.Type.ERROR_MESSAGE);
			return null;
		}

		return fileOutputStream;
	}

	@Override
	public void uploadSucceeded(SucceededEvent event) {
		long size = this.file.length();
		
		if(this.checkFileName(this.file.getName(), "overhead")) {
			size = size + 4;
		}
		this.fileList.add(new Files(this.file.getPath(), size));
		this.grid.getDataProvider().refreshAll();
	}

	@Override
	public void uploadFailed(FailedEvent event) {

	}

	private boolean checkFileName(String filename, String pattern) {
		if (filename.contains(pattern)) {
			return true;
		}
		
		return false;
	}

}
