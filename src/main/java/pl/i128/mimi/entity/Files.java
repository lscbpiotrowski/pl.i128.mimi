package pl.i128.mimi.entity;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode
public class Files implements Serializable {

	private static final long serialVersionUID = 2501337300925909549L;

	@NonNull
	private String path;
	
	@NonNull
	private long size;

}
