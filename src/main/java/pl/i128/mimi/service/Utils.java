package pl.i128.mimi.service;

import java.io.File;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import lombok.Getter;
import pl.i128.mimi.entity.Files;

public class Utils {

	private static final String OVERHEAD = "overhead";
	private static final long VALUE = 4;

	@Getter
	private long fullSize = 0;

	public void checkExistFilesAndGenerateSize(Set<Files> files) {
		Iterator<Files> iterator = files.iterator();

		while (iterator.hasNext()) {
			Files item = iterator.next();
			File file = new File(item.getPath());

			if (this.checkNotExistFile(file)) {
				iterator.remove();
			} else {
				item.setSize(this.generateSize(file, item));
				this.fullSize = this.fullSize + item.getSize();
			}

		}
	}

	private boolean checkNotExistFile(File file) {
		if (file.isDirectory() || !file.exists()) {
			return true;
		}

		return false;
	}

	private long generateSize(File file, Files item) {

		long size = file.length();

		if (this.checkFileName(item.getPath(), OVERHEAD)) {
			return size + VALUE;
		}

		return size;
	}

	private boolean checkFileName(String filename, String pattern) {
		if (filename.contains(pattern)) {
			return true;
		}

		return false;
	}

}
