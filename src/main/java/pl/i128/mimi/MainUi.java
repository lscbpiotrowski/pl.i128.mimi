package pl.i128.mimi;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.event.MouseEvents.ClickListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;

import pl.i128.mimi.entity.Files;
import pl.i128.mimi.receiver.FileReceiver;
import pl.i128.mimi.service.Utils;

/**
 * This UI is the application entry point. A UI may either represent a browser
 * window (or tab) or some part of an HTML page where a Vaadin application is
 * embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is
 * intended to be overridden to add component to the user interface and
 * initialize non-component functionality.
 */
@Theme("mytheme")
public class MainUi extends UI {

	private Set<Files> fileList = new HashSet<>();
	private Grid<Files> grid = new Grid<>();
	private Utils utils = new Utils();

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		final VerticalLayout layout = new VerticalLayout();

		layout.addComponent(new Label("UPLOAD FILE CALCULATOR MIMI"));

		TextField textField = new TextField("Path Input File");
		textField.setCaption("Type you file destinity");

		Button button = new Button("Add Path to list");

		button.addClickListener(e -> {
			if (!textField.getValue().isEmpty()) {
				fileList.add(new Files(textField.getValue(), 0));
				grid.getDataProvider().refreshAll();
			}
		});

		layout.addComponent(textField);
		layout.addComponent(button);
		
		Label totalValueLabel = new Label("0");
		totalValueLabel.setCaption("Total Size");
		layout.addComponent(totalValueLabel);

		Button generateButton = new Button("Count");

		generateButton.addClickListener(e -> {
			utils.checkExistFilesAndGenerateSize(fileList);
			grid.getDataProvider().refreshAll();
			totalValueLabel.setValue(String.valueOf(utils.getFullSize()));
		});

		layout.addComponent(generateButton);
		
		// FileReceiver receiver = new FileReceiver(fileList, grid);
		//
		// Upload upload = new Upload("Upload File", receiver);
		// upload.setImmediateMode(true);
		// upload.setButtonCaption("UPLOAD");
		// upload.addSucceededListener(receiver);
		// upload.addFailedListener(receiver);
		//
		// layout.addComponent(upload);
		//
		grid.setItems(fileList);
		//
		grid.addColumn(Files::getPath).setCaption("Path");
		grid.addColumn(Files::getSize).setCaption("Size");
		//

		layout.addComponent(grid);

		// final TextField name = new TextField();
		// name.setCaption("Type your name here:");
		//
		// Button button = new Button("Click Me");
		// button.addClickListener(e -> {
		// layout.addComponent(new Label("Thanks " + name.getValue()
		// + ", it works!"));
		// });
		//
		// layout.addComponents(name, button);

		setContent(layout);
	}

	@WebServlet(urlPatterns = "/*", name = "mainServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = MainUi.class, productionMode = false)
	public static class mainServlet extends VaadinServlet {
	}
}
